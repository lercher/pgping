package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/jackc/pgx/v4"
	_ "github.com/jackc/pgx/v4/stdlib"
)

func main() {
	log.Println("This is pgping, (C) 2022 by Martin Lercher")

	cs := os.ExpandEnv("postgres://postgres:${pgpass}@linux-pm81:5432/postgres?statement_cache_capacity=0")
	cfg, err := pgx.ParseConfig(cs)
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("Connecting to %s with database %s", cfg.Host, cfg.Database)

	db, err := sql.Open("pgx", cs)
	if err != nil {
		log.Fatalln(err)
	}

	scanSingle[string](db, "Version:", "SELECT version();")
	scanSingle[int](db, "The answer is", "SELECT 6*7 as i;")

}

func scanSingle[T any](db *sql.DB, label, s string) {
	rows, err := db.Query(s)
	if err != nil {
		log.Fatalln(err)
	}
	defer rows.Close()

	var v T
	for rows.Next() {
		err = rows.Scan(&v)
		if err != nil {
			log.Println(err)
			continue
		}
		fmt.Println(label, v)
	}
}