# pgping

pgping connects to a PostgreSQL database in a container
and selects the version and an integer from it.

## running the program

```log
C:\git\src\gitlab.com\lercher\pgping>set pgpass=sth-secret

C:\git\src\gitlab.com\lercher\pgping>go run .
2022/03/22 21:14:22 This is pgping, (C) 2022 by Martin Lercher
2022/03/22 21:14:22 Connecting to linux-pm81 with database postgres
Version: PostgreSQL 14.2 on x86_64-pc-linux-musl, compiled by gcc (Alpine 10.3.1_git20211027) 10.3.1 20211027, 64-bit
The answer is 42

C:\git\src\gitlab.com\lercher\pgping>
```

## podman part

```log
lercher@linux-pm81:~/pg> podman run --name mypostgres -e POSTGRES_PASSWORD=... -e PGDATA=/var/lib/postgresql/data/pgdata -v ./data:/var/lib/postgresql/data/pgdata -p 5432:5432 postgres:alpine
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "en_US.utf8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are disabled.

fixing permissions on existing directory /var/lib/postgresql/data/pgdata ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default time zone ... UTC
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... sh: locale: not found
2022-03-22 19:07:54.232 UTC [31] WARNING:  no usable system locales were found
ok
syncing data to disk ... ok

initdb: warning: enabling "trust" authentication for local connections
You can change this by editing pg_hba.conf or using the option -A, or
--auth-local and --auth-host, the next time you run initdb.

Success. You can now start the database server using:

    pg_ctl -D /var/lib/postgresql/data/pgdata -l logfile start

waiting for server to start....2022-03-22 19:07:55.161 UTC [37] LOG:  starting PostgreSQL 14.2 on x86_64-pc-linux-musl, compiled by gcc (Alpine 10.3.1_git20211027) 10.3.1 20211027, 64-bit
2022-03-22 19:07:55.171 UTC [37] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
2022-03-22 19:07:55.184 UTC [38] LOG:  database system was shut down at 2022-03-22 19:07:54 UTC
2022-03-22 19:07:55.187 UTC [37] LOG:  database system is ready to accept connections
 done
server started

/usr/local/bin/docker-entrypoint.sh: ignoring /docker-entrypoint-initdb.d/*

waiting for server to shut down....2022-03-22 19:07:55.267 UTC [37] LOG:  received fast shutdown request
2022-03-22 19:07:55.276 UTC [37] LOG:  aborting any active transactions
2022-03-22 19:07:55.276 UTC [37] LOG:  background worker "logical replication launcher" (PID 44) exited with exit code 1
2022-03-22 19:07:55.277 UTC [39] LOG:  shutting down
2022-03-22 19:07:55.297 UTC [37] LOG:  database system is shut down
 done
server stopped

PostgreSQL init process complete; ready for start up.

2022-03-22 19:07:55.392 UTC [1] LOG:  starting PostgreSQL 14.2 on x86_64-pc-linux-musl, compiled by gcc (Alpine 10.3.1_git20211027) 10.3.1 20211027, 64-bit
2022-03-22 19:07:55.393 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
2022-03-22 19:07:55.393 UTC [1] LOG:  listening on IPv6 address "::", port 5432
2022-03-22 19:07:55.399 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
2022-03-22 19:07:55.410 UTC [49] LOG:  database system was shut down at 2022-03-22 19:07:55 UTC
2022-03-22 19:07:55.414 UTC [1] LOG:  database system is ready to accept connections
^C2022-03-22 19:08:38.548 UTC [1] LOG:  received fast shutdown request
2022-03-22 19:08:38.554 UTC [1] LOG:  aborting any active transactions
2022-03-22 19:08:38.556 UTC [1] LOG:  background worker "logical replication launcher" (PID 55) exited with exit code 1
2022-03-22 19:08:38.556 UTC [50] LOG:  shutting down
2022-03-22 19:08:38.583 UTC [1] LOG:  database system is shut down
lercher@linux-pm81:~/pg>

lercher@linux-pm81:~/pg> podman start mypostgres
mypostgres
lercher@linux-pm81:~/pg> podman ps
CONTAINER ID  IMAGE                              COMMAND     CREATED         STATUS            PORTS                   NAMES
a640061b306f  docker.io/library/postgres:alpine  postgres    10 minutes ago  Up 7 seconds ago  0.0.0.0:5432->5432/tcp  mypostgres
lercher@linux-pm81:~/pg>
```
